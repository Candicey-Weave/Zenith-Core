rootProject.name = "Zenith-Core"

pluginManagement {
    repositories {
        gradlePluginPortal()
        maven("https://repo.polyfrost.org/releases")
        maven("https://jitpack.io")
    }
}