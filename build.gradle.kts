import cc.polyfrost.gradle.util.RelocationTransform.Companion.registerRelocationAttribute
import cc.polyfrost.gradle.util.prebundle
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.commons.Remapper
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LdcInsnNode
import java.io.FileOutputStream
import java.util.jar.JarEntry
import java.util.jar.JarFile
import java.util.jar.JarOutputStream

plugins {
    kotlin("jvm") version ("1.8.0")

    id("cc.polyfrost.multi-version.root") version ("0.2.5")

    `java-library`
    `maven-publish`

    id("com.github.weave-mc.weave") version ("8b70bcc707")
}

val PACKAGE = "com.gitlab.candicey.zenithcore"
val ZENITH_CORE_VERSION = "1.4.1"

group = PACKAGE
version = ZENITH_CORE_VERSION

minecraft.version("1.8.9")

val gitlabProjectId = 45235852

repositories {
    mavenCentral()
    maven("https://jitpack.io")
    maven("https://repo.spongepowered.org/maven")

    maven {
        url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
        name = "GitLab"
        credentials(HttpHeaderCredentials::class) {
            name = "Private-Token"
            value = findProperty("gitLabPrivateToken") as String?
        }
        authentication {
            create("header", HttpHeaderAuthentication::class)
        }
    }
}

val relocated = registerRelocationAttribute("relocate") {
    relocate("com.google", "com.gitlab.candicey.zenithcore.libs")
}

val shadeRelocated: Configuration by configurations.creating {
    attributes { attribute(relocated, true) }
}

val shade: Configuration by configurations.creating {
    configurations.api.get().extendsFrom(this)
}

dependencies {
    testImplementation(kotlin("test"))

    implementation("com.google.code.gson:gson:2.10.1")

    implementation("org.ow2.asm:asm:9.4")
    implementation("org.ow2.asm:asm-commons:9.4")
    implementation("org.ow2.asm:asm-tree:9.4")
    implementation("org.ow2.asm:asm-util:9.4")

    api("org.jetbrains.kotlin:kotlin-reflect:1.7.20")

    compileOnly("com.github.Weave-MC:Weave-Loader:v0.2.3")
    compileOnly("org.spongepowered:mixin:0.8.5")

    implementation(prebundle(shadeRelocated))
}

java {
    withSourcesJar()
    withJavadocJar()
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

val relocate: TaskProvider<Task> = tasks.register("relocate") {
    val originalJar = project.tasks.getByName("jar").outputs.files.singleFile

    doLast {
        val jar = JarFile(originalJar)
        val entries = jar.entries()
        val newJar = JarOutputStream(FileOutputStream(File(originalJar.parentFile, "${originalJar.nameWithoutExtension}-relocated.jar")))

        val classNameReplaceList = mapOf<String, String>(
            "com/gitlab/candicey/zenithcore" to "com/gitlab/candicey/zenithcore_v${ZENITH_CORE_VERSION.replace('.', '_')}",
        )

        val stringReplaceList = mapOf<String, String>(
            "@@ZENITH_CORE_VERSION@@" to ZENITH_CORE_VERSION,
        )

        val writeEntryToFile = { file: JarFile, outStream: JarOutputStream, entry: JarEntry, entryName: String ->
            outStream.putNextEntry(JarEntry(entryName))
            outStream.write(file.getInputStream(entry).readBytes())
            outStream.closeEntry()
        }

        while (entries.hasMoreElements()) {
            val entry = entries.nextElement()
            // only modify classes
            if (!entry.isDirectory) {
                when {
                    entry.name.startsWith(PACKAGE.replace('.', '/')) -> {
                        val bytes = jar.getInputStream(entry).readBytes()

                        var cr = ClassReader(bytes)
                        var cw = ClassWriter(cr, 0)
                        cr.accept(ClassRemapper(cw, object : Remapper() {
                            override fun map(internalName: String): String =
                                classNameReplaceList.entries.fold(internalName) { acc, (target, replacement) -> acc.replaceFirst(target, replacement) }
                        }), 0)

                        cr = ClassReader(cw.toByteArray())
                        val cn = ClassNode()
                        cr.accept(cn, 0)

                        for (method in cn.methods) {
                            for (insn in method.instructions) {
                                if (insn is LdcInsnNode) {
                                    if (insn.cst is String) {
                                        val cst = insn.cst as? String ?: continue
                                        if (stringReplaceList.containsKey(cst)) {
                                            val newCst = stringReplaceList[cst]
                                            if (newCst != null) {
                                                insn.cst = newCst
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        cw = ClassWriter(cr, 0)
                        cn.accept(cw)

                        newJar.putNextEntry(JarEntry(classNameReplaceList.entries.fold(entry.name) { acc, (target, replacement) -> acc.replaceFirst(target, replacement) }))
                        newJar.write(cw.toByteArray())
                        newJar.closeEntry()
                    }

                    entry.name == "weave.mod.json" -> {
                        val bytes = jar.getInputStream(entry).readBytes()
                        var string = String(bytes)

                        for ((target, replacement) in classNameReplaceList) {
                            val dotTarget = target.replace('/', '.')
                            val dotReplacement = replacement.replace('/', '.')

                            string = string.replace(dotTarget, dotReplacement)
                        }

                        newJar.putNextEntry(JarEntry(entry.name))
                        newJar.write(string.toByteArray())
                        newJar.closeEntry()
                    }

                    else -> writeEntryToFile(jar, newJar, entry, entry.name)
                }
            }
        }

        newJar.close()
    }

    outputs.file(originalJar.parentFile.resolve("${originalJar.nameWithoutExtension}-relocated.jar"))
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])

            artifact(tasks.getByName("relocate").outputs.files.singleFile) {
                classifier = "relocated"
            }
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

tasks.getByName("jar").finalizedBy(tasks.getByName("relocate"))
tasks.getByName("publish").dependsOn(tasks.getByName("build"))