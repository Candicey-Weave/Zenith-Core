package com.gitlab.candicey.zenithcore.test

import com.gitlab.candicey.zenithcore.helper.LunarClassHelper

object TestMain {
    @JvmStatic
    fun main(args: Array<String>) {
        val startTime = System.currentTimeMillis()

        val lunar = LunarClassHelper.classBytes["lunar.jar"]!!
//        println(lunar.map { it.key })
        println("Size: ${lunar.size}")

        println("Time: ${System.currentTimeMillis() - startTime}ms")
    }
}