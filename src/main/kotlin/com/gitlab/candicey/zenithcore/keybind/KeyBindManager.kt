package com.gitlab.candicey.zenithcore.keybind

import com.gitlab.candicey.zenithcore.event.LateKeyboardEvent
import net.minecraft.client.settings.KeyBinding
import net.weavemc.loader.api.event.KeyboardEvent
import net.weavemc.loader.api.event.SubscribeEvent

/**
 * Manages custom key bindings.
 *
 * @see [com.gitlab.candicey.zenithcore.hook.GameSettingsHook]
 * @see [com.gitlab.candicey.zenithcore.hook.LocaleHook]
 */
object KeyBindManager {
    /**
     * Key bindings that are triggered before the game processes the key press.
     *
     * @see [com.gitlab.candicey.zenithcore.hook.LocaleHook]
     */
    val keyBindings: MutableList<KeyBind> = mutableListOf(
        // KeyBind("key descriptor", key, "category")
        // KeyBind("zenithcore.keybind.test", Keyboard.KEY_0, "Zenith Core") { _, _ -> println("test") }
    )

    /**
     * Key bindings that are triggered after the game processes the key press.
     *
     * @see [com.gitlab.candicey.zenithcore.hook.LocaleHook]
     */
    val lateKeyBindings: MutableList<KeyBind> = mutableListOf(
        // KeyBind("key descriptor", key, "category")
        // KeyBind("zenithcore.keybind.test", Keyboard.KEY_0, "Zenith Core") { _, _ -> println("test") }
    )

    @SubscribeEvent
    fun onKeyboardEvent(event: KeyboardEvent) {
        keyBindings
            .filterNot { it.onlyActivateWhenKeyPressed && !it.isPressed }
            .forEach { it.onEvent?.invoke(it, event) }
    }

    @SubscribeEvent
    fun onLateKeyboardEvent(event: LateKeyboardEvent) {
        lateKeyBindings
            .filterNot { it.onlyActivateWhenKeyPressed && !it.isPressed }
            .forEach { it.onEvent?.invoke(it, KeyboardEvent()) }
    }
}

/**
 *
 */
class KeyBind(
    val name: String,
    @get:JvmName("getKeyBindCode")
    val keyCode: Int,
    val category: String,
    val onlyActivateWhenKeyPressed: Boolean = true,
    val onEvent: ((KeyBind, KeyboardEvent) -> Unit)? = null
) : KeyBinding(
    name,
    keyCode,
    category,
)