package com.gitlab.candicey.zenithcore.command

import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zenithcore.util.*
import net.minecraft.event.ClickEvent
import net.minecraft.event.HoverEvent

/**
 * The super class of all commands.
 *
 * @property commandInitialisationData The data for initialising the command.
 * @see [CommandInitialisationData]
 * @see [CommandManager]
 */
abstract class CommandAbstract(private val commandInitialisationData: CommandInitialisationData) {
    /**
     * If true, the sub commands will be run.
     */
    var runSubCommand = false

    /**
     * If true, the [postExecute] will be run.
     */
    var runPostExecute = false

    /**
     * If true, the [preRunSubCommand] will be run.
     */
    var runPreSubCommand = false

    /**
     * The list of sub commands.
     */
    open val subCommands: MutableList<CommandAbstract> = mutableListOf()

    /**
     * The info of this command.
     */
    var commandInfo: CommandInfo

    /**
     * If true, this command will be hidden from the help message.
     */
    var hideCommand = false

    /**
     * The aliases of this command.
     */
    var aliases: Array<String>

    /**
     * The description of this command.
     */
    var description: String

    init {
        commandInfo = javaClass.getAnnotation(CommandInfo::class.java) ?: throw IllegalArgumentException("Command class must be annotated with @CommandInfo (class: ${javaClass.name})")
        hideCommand = javaClass.getAnnotation(HideCommand::class.java) != null
        aliases = commandInfo.aliases.toList().toTypedArray()
        description = commandInfo.description
    }

    /**
     * Run before [execute] and managing all things in this command.
     *
     * @param args The arguments of the command.
     * @param rawMessage The raw message of the command.
     * @param previousCommandName An array of the previous command names.
     */
    fun preExecute(args: List<String>, rawMessage: String, previousCommandName: Array<String>) {
        runSubCommand = false
        runPostExecute = false
        runPreSubCommand = false

        execute(args, rawMessage, previousCommandName)

        if (runSubCommand) {
            val nextArgsArray = args.toTypedArray().copyOfRange(1, args.size)
            val nextCommandName = args[0]

            val filter = subCommands
                .filter { it.isThisCommand(nextCommandName) }
                .toMutableList()

            if (filter.isEmpty()) {
                commandInitialisationData
                    .addPrefixFunction("$RED${BOLD}Command not found!")
                    .toChatComponent()
                    .addChatMessage()
            } else {
                if (runPreSubCommand) {
                    preRunSubCommand(filter)
                }
                filter.forEach {
                    it.preExecute(nextArgsArray.toList(), rawMessage, previousCommandName.plus(aliases[0]))
                }
            }

            if (runPostExecute) {
                postExecute()
            }
        }
    }

    /**
     * Execute the command.
     *
     * @param args The arguments of the command.
     * @param rawMessage The raw message of the command.
     * @param previousCommandName An array of the previous command names.
     */
    open fun execute(args: List<String>, rawMessage: String, previousCommandName: Array<String>) {
        if (args.isEmpty()) {
            sendHelpMessage(previousCommandName.plus(aliases[0]))
        } else {
            runSubCommand = true
        }
    }

    /**
     * Run after running sub commands.
     */
    open fun postExecute() {
    }

    /**
     * Run before running sub commands.
     *
     * @param subCommands The sub commands that will be run.
     */
    open fun preRunSubCommand(subCommands: MutableList<CommandAbstract>) {
    }

    /**
     * Check if the message matches this command.
     *
     * @param command The message to check.
     * @return If the message matches this command.
     */
    fun isThisCommand(command: String): Boolean {
        aliases.forEach {
            if (it.equals(command, true)) {
                return true
            }
        }
        return false
    }

    /**
     * Send help message to the player. All the commands that are not hidden will be displayed.
     *
     * @param previousCommandName All the previous command names.
     */
    open fun sendHelpMessage(previousCommandName: Array<String>) {
        commandInitialisationData
            .addPrefixFunction("$YELLOW${BOLD}${UNDERLINE}Available commands$YELLOW${BOLD}:")
            .toChatComponent()
            .addChatMessage()

        subCommands.forEach { commandAbstract ->
            val prefix = commandInitialisationData.prefix
            val commandName = commandInitialisationData.commandName[0]
            val alias = commandAbstract.aliases[0]
            if (!commandAbstract::class.java.annotations.map { it::class.java }.contains(HideCommand::class.java)) {
                val iChatComponent = commandInitialisationData
                    .addPrefixFunction(" $GREY$BOLD$RIGHT_ARROW$RESET ")
                    .toChatComponent()

                val command = "$prefix${commandName}${if (previousCommandName.isNotEmpty()) " ${previousCommandName.joinToString(" ")}" else ""} $alias"
                val aliasComponent = "$DARK_AQUA$alias".toChatComponent().apply {
                    chatStyle.apply {
                        chatClickEvent = ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, command)
                        chatHoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, command.toChatComponent())
                    }
                }
                iChatComponent.appendSibling(aliasComponent)

                if (commandAbstract.description.isNotBlank()) {
                    iChatComponent
                        .appendSibling(" $YELLOW- ${commandAbstract.description}".toChatComponent())
                }

                iChatComponent.addChatMessage()
            }
        }
    }

    /**
     * If the [args] is empty, send help message.
     *
     * @param args The arguments of the command.
     * @param previousCommandName All the previous command names.
     */
    open fun checkArgs(args: List<String>, previousCommandName: Array<String>) {
        if (args.isEmpty()) {
            sendHelpMessage(previousCommandName.plus(aliases[0]))
        }
    }
}