package com.gitlab.candicey.zenithcore.command

/**
 * Used to store the info of a command.
 *
 * @property aliases The aliases of the command.
 * @property description The description of the command.
 */
annotation class CommandInfo(
    vararg val aliases: String,
    val description: String = "",
)
