package com.gitlab.candicey.zenithcore.command

/**
 * Used to hide a command from the help message.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class HideCommand