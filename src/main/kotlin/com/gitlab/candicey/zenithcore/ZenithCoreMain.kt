package com.gitlab.candicey.zenithcore

import com.gitlab.candicey.zenithcore.extension.add
import com.gitlab.candicey.zenithcore.font.FontManager
import com.gitlab.candicey.zenithcore.helper.*
import com.gitlab.candicey.zenithcore.hook.*
import com.gitlab.candicey.zenithcore.keybind.KeyBindManager
import net.weavemc.loader.api.ModInitializer
import net.weavemc.loader.api.event.EventBus
import net.weavemc.loader.api.event.StartGameEvent

class ZenithCoreMain : ModInitializer {
    override fun preInit() {
        info("Initialising...")
        
        FileHelper.cleanMotherTempDirectory()
        Runtime.getRuntime().addShutdownHook(Thread(FileHelper::deleteTempDirectory))

        info("Registering hooks...")
        HookManagerHelper.hooks.add(
            GL11Hook,
            GuiScreenHook,
            GuiConnectingHook,
            GameSettingsHook,
            LocaleHook,
            ChatComponentTextHook,
            ChatStyleHook,
            ClickEventHook,
            ClickEventActionHook,
        )
        info("Hooks registered!")

        info("Subscribing events...")
        EventBus.subscribe(StartGameEvent.Post::class.java) {
            EventBus.subscribe(KeyBindManager)
            EventBus.subscribe(MessageHelper)
            EventBus.subscribe(ScaledResolutionHelper)
            FontManager.waitUntilReady()
        }
        info("Events subscribed!")

        info("Initialisation complete!")
    }
}