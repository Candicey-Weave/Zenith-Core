package com.gitlab.candicey.zenithcore.event

import net.minecraft.entity.Entity
import net.minecraft.world.World
import net.weavemc.loader.api.event.CancellableEvent
import net.weavemc.loader.api.event.Event

/**
 * Called when the client ticks.
 *
 * @see [com.gitlab.candicey.zenithcore.event.hook.TickEventHook]
 */
sealed class TickEvent : Event() {
    object Pre : TickEvent()

    object Post: TickEvent()
}

/**
 * Called when the player ticks.
 *
 * @see [com.gitlab.candicey.zenithcore.event.hook.PlayerTickEventHook]
 */
sealed class PlayerTickEvent : Event() {
    object Pre : PlayerTickEvent()

    object Post : PlayerTickEvent()
}

/**
 * Called when the entity joins the world.
 *
 * @see [com.gitlab.candicey.zenithcore.event.hook.EntityJoinWorldEventHook]
 */
class EntityJoinWorldEvent(val entity: Entity, val world: World) : CancellableEvent()

/**
 * Called after the game processes the key press.
 *
 * @see [com.gitlab.candicey.zenithcore.event.hook.LateKeyboardEventHook]
 */
class LateKeyboardEvent : Event()