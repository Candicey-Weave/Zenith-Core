package com.gitlab.candicey.zenithcore.event.hook

import com.gitlab.candicey.zenithcore.event.TickEvent
import com.gitlab.candicey.zenithcore.util.weave.callEvent
import com.gitlab.candicey.zenithcore.util.weave.getSingleton
import com.gitlab.candicey.zenithcore.util.weave.named
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode

/**
 * A [com.gitlab.candicey.zenithcore.event.TickEvent] is posted every tick.
 *
 * @see [net.minecraft.util.Timer.ticksPerSecond]
 * @author Weave-MC
 */
class TickEventHook : Hook("net/minecraft/client/Minecraft") {
    /**
     * Inserts a call to the [net.minecraft.client.Minecraft.runTick] method to post a 'tick'.
     *
     * @see [net.minecraft.client.Minecraft.runTick]
     */
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val runTick = node.methods.named("runTick")

        runTick.instructions.insert(asm {
            getSingleton<TickEvent.Pre>()
            callEvent()
        })

        runTick.instructions.insertBefore(
            runTick.instructions.findLast { it.opcode == Opcodes.RETURN },
            asm {
                getSingleton<TickEvent.Post>()
                callEvent()
            }
        )
    }
}