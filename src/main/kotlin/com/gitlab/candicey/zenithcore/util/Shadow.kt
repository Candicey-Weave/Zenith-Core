package com.gitlab.candicey.zenithcore.util

import kotlin.reflect.KProperty

class DelegateHolder<T>(private var `object`: T) {
    operator fun getValue(instance: Any, kProperty: KProperty<*>): T = `object`

    operator fun setValue(instance: Any, kProperty: KProperty<*>, value: T) {
        `object` = value
    }

    operator fun getValue(nothing: Nothing?, kProperty: KProperty<*>): T = `object`

    operator fun setValue(nothing: Nothing?, kProperty: KProperty<*>, value: T) {
        `object` = value
    }
}

class ShadowField<T>(private val ownerClass: Class<*>? = null, private val variableName: String? = null, private val instance: Any? = null) {
    private var fieldCache: FieldCache? = null

    operator fun getValue(instance: Any, kProperty: KProperty<*>): T =
        getFieldCache(ownerClass ?: instance::class.java, variableName ?: kProperty.name).get(this.instance ?: instance) as T

    operator fun setValue(instance: Any, kProperty: KProperty<*>, value: T) =
        getFieldCache(ownerClass ?: instance::class.java, variableName ?: kProperty.name).set(this.instance ?: instance, value)

    private fun getFieldCache(ownerClass: Class<*>, fieldName: String): FieldCache =
        if (fieldCache == null) {
            reflectField(ownerClass, fieldName).also { fieldCache = it }
        } else {
            fieldCache!!
        }
}