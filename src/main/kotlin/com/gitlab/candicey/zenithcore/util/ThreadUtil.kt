package com.gitlab.candicey.zenithcore.util

import java.util.concurrent.*

private val executors: ExecutorService = Executors.newFixedThreadPool(50) ?: error("Can't create new ExecutorService")

private val scheduledExecutors: ScheduledExecutorService = Executors.newScheduledThreadPool(30) ?: error("Can't create new ScheduledExecutorService")

/**
 * Runs a block of code in a new thread asynchronously.
 *
 * @param block Block of code you want to run.
 * @return [Future] of the block of code.
 */
fun runAsync(block: () -> Unit): Future<*> = executors.submit(block)

/**
 * Runs a block of code in a new thread asynchronously after a delay.
 *
 * @param delay Delay time in milliseconds before running the block of code.
 * @param timeUnit Time unit of the delay time. (Default: [TimeUnit.MILLISECONDS])
 * @param block Block of code you want to run.
 * @return [Future] of the block of code.
 */
fun runDelayedAsync(delay: Long, timeUnit: TimeUnit = TimeUnit.MILLISECONDS, block: () -> Unit): Future<*> = scheduledExecutors.schedule(block, delay, timeUnit)