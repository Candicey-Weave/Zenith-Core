package com.gitlab.candicey.zenithcore.util

import com.gitlab.candicey.zenithcore.mc
import net.minecraft.network.Packet

/**
 * Sends a packet to the server.
 * @param packet The packet to send.
 */
fun sendPacket(packet: Packet<*>) = mc.netHandler.addToSendQueue(packet)