package com.gitlab.candicey.zenithcore.util.weave

import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Type
import org.objectweb.asm.tree.AbstractInsnNode
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode
import org.objectweb.asm.tree.MethodNode
import java.io.File

fun List<MethodNode>.named(name: String) = find { it.name == name }!!
fun List<FieldNode>.named(name: String) = find { it.name == name }!!

inline fun <reified T : Any> internalNameOf(): String = Type.getInternalName(T::class.java)

inline fun <reified T : AbstractInsnNode> AbstractInsnNode.next(p: (T) -> Boolean = { true }): T? {
    var insn = this

    while (true) {
        insn = insn.next ?: return null
        if (insn is T && p(insn)) return insn
    }
}

inline fun <reified T : AbstractInsnNode> AbstractInsnNode.prev(p: (T) -> Boolean = { true }): T? {
    var insn = this

    while (true) {
        insn = insn.previous ?: return null
        if (insn is T && p(insn)) return insn
    }
}

fun ClassNode.dump(file: String) {
    val cw = ClassWriter(0)
    accept(cw)
    File(file).writeBytes(cw.toByteArray())
}
