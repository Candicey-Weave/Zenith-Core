package com.gitlab.candicey.zenithcore.util

import com.gitlab.candicey.zenithcore.extension.getAllField
import com.gitlab.candicey.zenithcore.extension.getAllMethod

/**
 * A map of class names to [Class] objects.
 */
private val classCache = mutableMapOf<String, Class<*>>()

/**
 * A list of [MethodCache]
 */
private val methodCache = mutableListOf<MethodCache>()

/**
 * A list of [FieldCache]
 */
private val fieldCache = mutableListOf<FieldCache>()

/**
 * Reflects a class and caches it for faster access later.
 *
 * @param className The name of the class to reflect. (e.g. "com.gitlab.candicey.zenithcore.util.ReflectionUtil" or "com/gitlab/candicey/zenithcore/util/ReflectionUtil")
 * @return The [Class] object.
 */
fun reflectClass(className: String): Class<*> {
    val replacedClassName = className.replace('/', '.')
    return classCache[replacedClassName] ?: Class.forName(replacedClassName).also { classCache[replacedClassName] = it }
}

/**
 * Reflects a method in the current class and caches it for faster access later.
 *
 * @param methodName The name of the method to reflect.
 * @param args The arguments of the method.
 * @return The [MethodCache] object.
 */
fun Any.reflectMethod(methodName: String, vararg args: Class<*>): MethodCache {
    return MethodCache.find(this::class.java, methodName, *args)
}

/**
 * Reflects a method in the specified class and caches it for faster access later.
 *
 * @param ownerClass The class that owns the method.
 * @param methodName The name of the method to reflect.
 * @param args The arguments of the method.
 * @return The [MethodCache] object.
 */
fun reflectMethod(ownerClass: Class<*>, methodName: String, vararg args: Class<*>): MethodCache {
    return MethodCache.find(ownerClass, methodName, *args)
}

/**
 * Reflects a method in the specified class and caches it for faster access later.
 *
 * @param ownerName The name of the class that owns the method. (e.g. "com.gitlab.candicey.zenithcore.util.ReflectionUtil" or "com/gitlab/candicey/zenithcore/util/ReflectionUtil")
 * @param methodName The name of the method to reflect.
 * @param args The arguments of the method.
 * @return The [MethodCache] object.
 */
fun reflectMethod(ownerName: String, methodName: String, vararg args: Class<*>): MethodCache {
    return MethodCache.find(ownerName, methodName, *args)
}

/**
 * Reflects a method in the current class and caches it for faster access later.
 *
 * @param T The class that owns the method.
 * @param methodName The name of the method to reflect.
 * @param args The arguments of the method.
 * @return The [MethodCache] object.
 */
inline fun <reified T> reflectMethod(methodName: String, vararg args: Class<*>): MethodCache {
    return reflectMethod(T::class.java, methodName, *args)
}

/**
 * Reflects a field in the current class and caches it for faster access later.
 *
 * @param fieldName The name of the field to reflect.
 * @return The [FieldCache] object.
 */
fun Any.reflectField(fieldName: String): FieldCache {
    return FieldCache.find(this::class.java, fieldName)
}

/**
 * Reflects a field in the specified class and caches it for faster access later.
 *
 * @param ownerClass The class that owns the field.
 * @param fieldName The name of the field to reflect.
 * @return The [FieldCache] object.
 */
fun reflectField(ownerClass: Class<*>, fieldName: String): FieldCache {
    return FieldCache.find(ownerClass, fieldName)
}

/**
 * Reflects a field in the specified class and caches it for faster access later.
 *
 * @param ownerName The name of the class that owns the field. (e.g. "com.gitlab.candicey.zenithcore.util.ReflectionUtil" or "com/gitlab/candicey/zenithcore/util/ReflectionUtil")
 * @param fieldName The name of the field to reflect.
 * @return The [FieldCache] object.
 */
fun reflectField(ownerName: String, fieldName: String): FieldCache {
    return FieldCache.find(ownerName, fieldName)
}

/**
 * Reflects a field in the current class and caches it for faster access later.
 *
 * @param T The class that owns the field.
 * @param fieldName The name of the field to reflect.
 * @return The [FieldCache] object.
 */
inline fun <reified T> reflectField(fieldName: String): FieldCache {
    return reflectField(T::class.java, fieldName)
}

/**
 * A data class that is used to cache the method.
 *
 * @property ownerClass The class that owns the method.
 * @property methodName The name of the method.
 * @property args The arguments of the method.
 */
data class MethodCache(val ownerClass: Class<*>, val methodName: String, val args: List<Class<*>>) {
    /**
     * The [java.lang.reflect.Method] object. It will be initialised lazily.
     */
    val method by lazy { ownerClass.getAllMethod(methodName, *args.toTypedArray())?.also { it.isAccessible = true } ?: throw NoSuchMethodException("$ownerClass.$methodName(${args.joinToString(", ") { it.name }})") }

    /**
     * Invoke the method.
     *
     * @param args The arguments of the method.
     */
    operator fun invoke(instance: Any? = null, vararg args: Any?): Any? {
        return method.invoke(instance, *args)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }

        if (other !is MethodCache) {
            return false
        }

        if (ownerClass != other.ownerClass) {
            return false
        }

        if (methodName != other.methodName) {
            return false
        }

        return args == other.args
    }

    override fun hashCode(): Int {
        var result = ownerClass.hashCode()
        result = 31 * result + methodName.hashCode()
        result = 31 * result + args.hashCode()
        return result
    }

    companion object {
        /**
         * Find the method in the cache. If it is not found, create a new one.
         *
         * @param ownerClass The class that owns the method.
         * @param methodName The name of the method.
         * @param args The arguments of the method.
         * @return The [MethodCache] object.
         */
        fun find(ownerClass: Class<*>, methodName: String, vararg args: Class<*>): MethodCache {
            return methodCache.find { it.ownerClass == ownerClass && it.methodName == methodName && it.args == args.toList() } ?: MethodCache(ownerClass, methodName, args.toList()).also { methodCache.add(it) }
        }

        /**
         * Find the method in the cache. If it is not found, create a new one.
         *
         * @param ownerName The name of the class that owns the method.
         * @param methodName The name of the method.
         * @param args The arguments of the method.
         * @return The [MethodCache] object.
         */
        fun find(ownerName: String, methodName: String, vararg args: Class<*>): MethodCache {
            return find(reflectClass(ownerName), methodName, *args)
        }
    }
}

/**
 * A data class that is used to cache the field.
 *
 * @property ownerClass The class that owns the field.
 * @property fieldName The name of the field.
 */
data class FieldCache(val ownerClass: Class<*>, val fieldName: String) {
    /**
     * The [java.lang.reflect.Field] object. It will be initialised lazily.
     */
    val field by lazy { ownerClass.getAllField(fieldName)?.also { it.isAccessible = true } ?: throw NoSuchFieldException("$ownerClass.$fieldName") }

    /**
     * Get the value of the field.
     *
     * @param instance The instance of the class. If it is null, it will be a static field.
     * @return The value of the field.
     */
    operator fun get(instance: Any? = null): Any? {
        return field.get(instance)
    }

    /**
     * Set the value of the field.
     *
     * @param instance The instance of the class. If it is null, it will be a static field.
     * @param value The value of the field.
     */
    operator fun set(instance: Any? = null, value: Any?) {
        field.set(instance, value)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }

        if (other !is FieldCache) {
            return false
        }

        if (ownerClass != other.ownerClass) {
            return false
        }

        return fieldName == other.fieldName
    }

    override fun hashCode(): Int {
        var result = ownerClass.hashCode()
        result = 31 * result + fieldName.hashCode()
        return result
    }

    companion object {
        /**
         * Find the field in the cache. If it is not found, create a new one.
         *
         * @param ownerClass The class that owns the field.
         * @param fieldName The name of the field.
         * @return The [FieldCache] object.
         */
        fun find(ownerClass: Class<*>, fieldName: String): FieldCache {
            return fieldCache.find { it.ownerClass == ownerClass && it.fieldName == fieldName } ?: FieldCache(ownerClass, fieldName).also { fieldCache.add(it) }
        }

        /**
         * Find the field in the cache. If it is not found, create a new one.
         *
         * @param ownerName The name of the class that owns the field.
         * @param fieldName The name of the field.
         * @return The [FieldCache] object.
         */
        fun find(ownerName: String, fieldName: String): FieldCache {
            return find(reflectClass(ownerName), fieldName)
        }
    }
}