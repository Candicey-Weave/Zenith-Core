package com.gitlab.candicey.zenithcore

internal fun info(message: String) = LOGGER.info("[Zenith-Core (${ZENITH_CORE_VERSION})] $message")
internal fun warn(message: String) = LOGGER.warn("[Zenith-Core (${ZENITH_CORE_VERSION})] $message")

/**
 * Checks if a string is a valid json.
 */
fun isJsonValid(json: String): Boolean = runCatching { JSON_PARSER.parse(json) }.isSuccess