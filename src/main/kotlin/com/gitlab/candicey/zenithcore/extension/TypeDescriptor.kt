package com.gitlab.candicey.zenithcore.extension

typealias TypeDescriptor = String

/**
 * Returns the size of the type on the stack.
 * If the type is a long or double, 2 is returned, otherwise 1.
 */
val TypeDescriptor.stackSize: Int
    get() = when (this) {
        "J", "D" -> 2
        else -> 1
    }