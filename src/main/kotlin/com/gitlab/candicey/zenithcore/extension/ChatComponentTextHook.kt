package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.util.ChatComponentText

var ChatComponentText.ignoreMessageHook: Boolean by ShadowField()