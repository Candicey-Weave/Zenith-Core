package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.util.reflectField
import net.minecraft.client.Minecraft
import net.minecraft.util.Session

private val sessionField = reflectField<Minecraft>("session")
fun Minecraft.setSession(value: Session) = sessionField.set(this, value)