package com.gitlab.candicey.zenithcore.extension

import java.lang.reflect.Method

/**
 * Gets the descriptor of a method.
 *
 * @return The descriptor of the method.
 * @see [Class.toDescriptor]
 */
val Method.descriptor: String
    get() {
        val parameterTypes = parameterTypes
        val parameterTypeDescriptors = parameterTypes.joinToString("") { it.toDescriptor() }
        val returnTypeDescriptor = returnType.toDescriptor()
        return "($parameterTypeDescriptors)$returnTypeDescriptor"
    }