package com.gitlab.candicey.zenithcore.extension

fun Int.toHex(): String = Integer.toHexString(this)