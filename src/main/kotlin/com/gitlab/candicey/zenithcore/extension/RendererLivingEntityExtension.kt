package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.renderer.entity.RenderPlayer
import net.minecraft.client.renderer.entity.RendererLivingEntity
import net.minecraft.client.renderer.entity.layers.LayerRenderer
import net.minecraft.entity.EntityLiving
import net.minecraft.entity.EntityLivingBase

var RendererLivingEntity<out EntityLivingBase>.layerRenderers: MutableList<LayerRenderer<*>> by ShadowField()