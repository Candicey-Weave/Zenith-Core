package com.gitlab.candicey.zenithcore.extension

fun <K, V> Iterable<Pair<K, V>>.toMutableMap(): MutableMap<K, V> = toMap().toMutableMap()

/**
 * Works like [forEach] but [init] is called for every element except the last one and [last] is called for the last element.
 *
 * @param runActionBefore If true, [action] is called before [init] and [last].
 */
fun <T> Iterable<T>.forEach(action: (T) -> Unit, init: (T) -> Unit = {}, last: (T) -> Unit = {}, runActionBefore: Boolean = true) =
    forEachIndexed { index, element ->
        if (runActionBefore) {
            action.invoke(element)

            if (index == count() - 1) {
                last.invoke(element)
            } else {
                init.invoke(element)
            }
        } else {
            if (index == count() - 1) {
                last.invoke(element)
            } else {
                init.invoke(element)
            }

            action.invoke(element)
        }
    }