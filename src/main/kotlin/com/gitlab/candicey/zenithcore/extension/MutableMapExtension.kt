package com.gitlab.candicey.zenithcore.extension

fun <K, V> MutableMap<K, V>.add(vararg pairs: Pair<K, V>) = pairs.forEach { (key, value) -> put(key, value) }