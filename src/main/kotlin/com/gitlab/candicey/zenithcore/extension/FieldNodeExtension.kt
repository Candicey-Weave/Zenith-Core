package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.warn
import org.objectweb.asm.tree.FieldNode

fun MutableList<FieldNode>.addFieldSafe(vararg fieldNode: FieldNode) {
    for (field in fieldNode) {
        if (any { it.name == field.name }) {
            warn("Field ${field.name} already exists in class.")
            continue
        }

        add(field)
    }
}