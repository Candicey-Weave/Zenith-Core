package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.enum.LogicalOperator
import com.gitlab.candicey.zenithcore.warn
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode

fun ClassNode.addFieldSafe(vararg fieldNode: FieldNode) {
    fieldNode.forEach {
        if (fields.map { field -> field.name }.contains(it.name)) {
            warn("Field ${it.name} already exists in class ${name}.")
            return@forEach
        }

        fields.add(it)
    }
}

/**
 * Checks if the class has any ldc instructions with the given string.
 *
 * @param string The string to check for.
 */
fun ClassNode.hasString(string: String): Boolean {
    return methods.any { it.hasString(string) }
}

/**
 * Checks if the class has [any][LogicalOperator.OR]/[all][LogicalOperator.AND] ldc instructions with the given strings.
 *
 * @param strings The strings to check for.
 * @param logicalOperator The logic operator to use.
 */
fun ClassNode.hasStrings(vararg strings: String, logicalOperator: LogicalOperator = LogicalOperator.AND): Boolean {
    return methods.map { it.stringLdc }.flatten().let { ldc ->
        when (logicalOperator) {
            LogicalOperator.OR -> strings.any { ldc.any { ldcConstant -> ldcConstant.value == it } }
            LogicalOperator.AND -> strings.all { ldc.any { ldcConstant -> ldcConstant.value == it } }
        }
    }
}