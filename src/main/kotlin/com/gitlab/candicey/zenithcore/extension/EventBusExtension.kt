package com.gitlab.candicey.zenithcore.extension

import net.weavemc.loader.api.event.EventBus

fun EventBus.subscribe(vararg events: Any) = events.forEach(EventBus::subscribe)