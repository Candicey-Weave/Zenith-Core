package com.gitlab.candicey.zenithcore.extension

import org.objectweb.asm.Opcodes.*
import org.objectweb.asm.tree.*

/**
 * Prints the [AbstractInsnNode] in a readable format.
 */
fun AbstractInsnNode.print() {
    when (this) {
        is FieldInsnNode -> {
            when (opcode) {
                GETFIELD -> println("GETFIELD $owner $name $desc")
                GETSTATIC -> println("GETSTATIC $owner $name $desc")
                PUTFIELD -> println("PUTFIELD $owner $name $desc")
                PUTSTATIC -> println("PUTSTATIC $owner $name $desc")
            }
        }

        is IincInsnNode -> {
            println("IINC $`var` $incr")
        }

        is InsnNode -> {
            when (opcode) {
                NOP -> println("NOP")
                ACONST_NULL -> println("ACONST_NULL")
                ICONST_M1 -> println("ICONST_M1")
                ICONST_0 -> println("ICONST_0")
                ICONST_1 -> println("ICONST_1")
                ICONST_2 -> println("ICONST_2")
                ICONST_3 -> println("ICONST_3")
                ICONST_4 -> println("ICONST_4")
                ICONST_5 -> println("ICONST_5")
                LCONST_0 -> println("LCONST_0")
                LCONST_1 -> println("LCONST_1")
                FCONST_0 -> println("FCONST_0")
                FCONST_1 -> println("FCONST_1")
                FCONST_2 -> println("FCONST_2")
                DCONST_0 -> println("DCONST_0")
                DCONST_1 -> println("DCONST_1")
                IALOAD -> println("IALOAD")
                LALOAD -> println("LALOAD")
                FALOAD -> println("FALOAD")
                DALOAD -> println("DALOAD")
                AALOAD -> println("AALOAD")
                BALOAD -> println("BALOAD")
                CALOAD -> println("CALOAD")
                SALOAD -> println("SALOAD")
                IASTORE -> println("IASTORE")
                LASTORE -> println("LASTORE")
                FASTORE -> println("FASTORE")
                DASTORE -> println("DASTORE")
                AASTORE -> println("AASTORE")
                BASTORE -> println("BASTORE")
                CASTORE -> println("CASTORE")
                SASTORE -> println("SASTORE")
                POP -> println("POP")
                POP2 -> println("POP2")
                DUP -> println("DUP")
                DUP_X1 -> println("DUP_X1")
                DUP_X2 -> println("DUP_X2")
                DUP2 -> println("DUP2")
                DUP2_X1 -> println("DUP2_X1")
                DUP2_X2 -> println("DUP2_X2")
                SWAP -> println("SWAP")
                IADD -> println("IADD")
                LADD -> println("LADD")
                FADD -> println("FADD")
                DADD -> println("DADD")
                ISUB -> println("ISUB")
                LSUB -> println("LSUB")
                FSUB -> println("FSUB")
                DSUB -> println("DSUB")
                IMUL -> println("IMUL")
                LMUL -> println("LMUL")
                FMUL -> println("FMUL")
                DMUL -> println("DMUL")
                IDIV -> println("IDIV")
                LDIV -> println("LDIV")
                FDIV -> println("FDIV")
                DDIV -> println("DDIV")
                IREM -> println("IREM")
                LREM -> println("LREM")
                FREM -> println("FREM")
                DREM -> println("DREM")
                INEG -> println("INEG")
                LNEG -> println("LNEG")
                FNEG -> println("FNEG")
                DNEG -> println("DNEG")
                ISHL -> println("ISHL")
                LSHL -> println("LSHL")
                ISHR -> println("ISHR")
                LSHR -> println("LSHR")
                IUSHR -> println("IUSHR")
                LUSHR -> println("LUSHR")
                IAND -> println("IAND")
                LAND -> println("LAND")
                IOR -> println("IOR")
                LOR -> println("LOR")
                IXOR -> println("IXOR")
                LXOR -> println("LXOR")
                I2L -> println("I2L")
                I2F -> println("I2F")
                I2D -> println("I2D")
                L2I -> println("L2I")
                L2F -> println("L2F")
                L2D -> println("L2D")
                F2I -> println("F2I")
                F2L -> println("F2L")
                F2D -> println("F2D")
                D2I -> println("D2I")
                D2L -> println("D2L")
                D2F -> println("D2F")
                I2B -> println("I2B")
                I2C -> println("I2C")
                I2S -> println("I2S")
                LCMP -> println("LCMP")
                FCMPL -> println("FCMPL")
                FCMPG -> println("FCMPG")
                DCMPL -> println("DCMPL")
                DCMPG -> println("DCMPG")
                IRETURN -> println("IRETURN")
                LRETURN -> println("LRETURN")
                FRETURN -> println("FRETURN")
                DRETURN -> println("DRETURN")
                ARETURN -> println("ARETURN")
                RETURN -> println("RETURN")
                ARRAYLENGTH -> println("ARRAYLENGTH")
                ATHROW -> println("ATHROW")
                MONITORENTER -> println("MONITORENTER")
                MONITOREXIT -> println("MONITOREXIT")
            }
        }

        is IntInsnNode -> {
            when (opcode) {
                BIPUSH -> println("BIPUSH $operand")
                SIPUSH -> println("SIPUSH $operand")
                NEWARRAY -> println("NEWARRAY $operand")
            }
        }

        is InvokeDynamicInsnNode -> {
            println("INVOKEDYNAMIC $name $desc $bsm ${bsmArgs.toList()}")
        }

        is JumpInsnNode -> {
            when (opcode) {
                IFEQ -> println("IFEQ $label")
                IFNE -> println("IFNE $label")
                IFLT -> println("IFLT $label")
                IFGE -> println("IFGE $label")
                IFGT -> println("IFGT $label")
                IFLE -> println("IFLE $label")
                IF_ICMPEQ -> println("IF_ICMPEQ $label")
                IF_ICMPNE -> println("IF_ICMPNE $label")
                IF_ICMPLT -> println("IF_ICMPLT $label")
                IF_ICMPGE -> println("IF_ICMPGE $label")
                IF_ICMPGT -> println("IF_ICMPGT $label")
                IF_ICMPLE -> println("IF_ICMPLE $label")
                IF_ACMPEQ -> println("IF_ACMPEQ $label")
                IF_ACMPNE -> println("IF_ACMPNE $label")
                GOTO -> println("GOTO $label")
                JSR -> println("JSR $label")
                IFNULL -> println("IFNULL $label")
                IFNONNULL -> println("IFNONNULL $label")
            }
        }

        is LabelNode -> {
            println("LABEL $label")
        }

        is LdcInsnNode -> {
            println("LDC $cst")
        }

        is LineNumberNode -> {
            println("LINENUMBER $line $start")
        }

        is LookupSwitchInsnNode -> {
            println("LOOKUPSWITCH $dflt $keys $labels")
        }

        is MethodInsnNode -> {
            when (opcode) {
                INVOKEVIRTUAL -> println("INVOKEVIRTUAL $owner $name $desc $itf")
                INVOKESPECIAL -> println("INVOKESPECIAL $owner $name $desc $itf")
                INVOKESTATIC -> println("INVOKESTATIC $owner $name $desc $itf")
                INVOKEINTERFACE -> println("INVOKEINTERFACE $owner $name $desc $itf")
            }
        }

        is MultiANewArrayInsnNode -> {
            println("MULTIANEWARRAY $desc $dims")
        }

        is TableSwitchInsnNode -> {
            println("TABLESWITCH $min $max $dflt $labels")
        }

        is TypeInsnNode -> {
            when (opcode) {
                NEW -> println("NEW $desc")
                ANEWARRAY -> println("ANEWARRAY $desc")
                CHECKCAST -> println("CHECKCAST $desc")
                INSTANCEOF -> println("INSTANCEOF $desc")
            }
        }

        is VarInsnNode -> {
            when (opcode) {
                ILOAD -> println("ILOAD $`var`")
                LLOAD -> println("LLOAD $`var`")
                FLOAD -> println("FLOAD $`var`")
                DLOAD -> println("DLOAD $`var`")
                ALOAD -> println("ALOAD $`var`")
                ISTORE -> println("ISTORE $`var`")
                LSTORE -> println("LSTORE $`var`")
                FSTORE -> println("FSTORE $`var`")
                DSTORE -> println("DSTORE $`var`")
                ASTORE -> println("ASTORE $`var`")
                RET -> println("RET $`var`")
            }
        }
    }
}