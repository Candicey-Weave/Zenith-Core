package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import io.netty.channel.Channel
import net.minecraft.network.NetworkManager

val NetworkManager.channel: Channel by ShadowField()