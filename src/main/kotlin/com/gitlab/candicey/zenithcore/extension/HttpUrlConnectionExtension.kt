package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.GSON
import java.net.HttpURLConnection

/**
 * Sets the default values for a [HttpURLConnection].
 *
 * Sets the request method to `GET`, the connect timeout to `5000` and the read timeout to `5000`.
 */
fun HttpURLConnection.setData(): HttpURLConnection {
    return apply {
        requestMethod = "GET"
        connectTimeout = 5000
        readTimeout = 5000
    }
}

/**
 * Gets the text from a [HttpURLConnection].
 */
fun HttpURLConnection.getText(): String {
    return (if (responseCode in 200..299) inputStream else errorStream).bufferedReader().readText()
}

/**
 * Gets the JSON from a [HttpURLConnection].
 *
 * @param T The type of the JSON.
 */
inline fun <reified T> HttpURLConnection.getJson(): T {
    return GSON.fromJson(getText(), T::class.java)
}