package com.gitlab.candicey.zenithcore.extension

import org.objectweb.asm.ClassReader
import org.objectweb.asm.tree.ClassNode

fun ByteArray.toClassNode() = ClassNode().also { ClassReader(this).accept(it, 0) }