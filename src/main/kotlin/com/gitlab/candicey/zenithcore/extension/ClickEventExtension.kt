package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.event.ClickEvent

var ClickEvent.runnable: Runnable? by ShadowField()