package com.gitlab.candicey.zenithcore.extension

fun <T> MutableList<T>.add(vararg elements: T) = addAll(elements)