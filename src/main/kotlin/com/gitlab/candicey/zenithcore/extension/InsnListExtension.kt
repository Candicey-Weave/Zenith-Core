package com.gitlab.candicey.zenithcore.extension

import org.objectweb.asm.Opcodes.RETURN
import org.objectweb.asm.tree.AbstractInsnNode
import org.objectweb.asm.tree.InsnList

/**
 * Inserts [insnList] before the last return instruction.
 */
fun InsnList.insertBeforeReturn(insnList: InsnList) {
    val returnInsn = findLast { it.opcode == RETURN } ?: return
    insertBefore(returnInsn, insnList)
}

fun InsnList.print() {
    forEach(AbstractInsnNode::print)
}