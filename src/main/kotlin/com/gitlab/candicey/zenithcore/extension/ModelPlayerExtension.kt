package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.model.ModelPlayer
import net.minecraft.client.model.ModelRenderer

var ModelPlayer.bipedDeadmau5Head: ModelRenderer by ShadowField()