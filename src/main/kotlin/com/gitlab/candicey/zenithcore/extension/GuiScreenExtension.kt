package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.util.reflectField
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiScreen

private val buttonListField = reflectField<GuiScreen>("buttonList")
fun GuiScreen.addButton(vararg guiButton: GuiButton) {
    (buttonListField.get(this) as MutableList<GuiButton>).addAll(guiButton)
}