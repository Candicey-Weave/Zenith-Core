package com.gitlab.candicey.zenithcore.config

import com.gitlab.candicey.zenithcore.util.DelegateHolder

/**
 * A class that manages [configs][Config].
 *
 * @see [Config]
 */
class ConfigManager() {
    /**
     * A map of all configs.
     *
     * The key is the name of the config (e.g. "keybinds"). The value is the [Config] object.
     */
    private val configs: MutableMap<String, Config<*>> = mutableMapOf()

    constructor(configs: MutableMap<String, Config<*>>) : this() {
        this.configs.putAll(configs)
    }

    /**
     * Loads all configs.
     */
    fun init() = configs.forEach { (_, config) -> config.loadConfig() }

    /**
     * Saves all configs.
     */
    fun save() = configs.forEach { (_, config) -> config.writeConfig() }

    /**
     * Gets a config from the [configs] map by the type [T].
     *
     * @param T The type of the config.
     * @return The config with the type [T] or null if not found.
     */
    inline fun <reified T : Any> getConfig(): Config<T>? =
        getConfigs().filter { it.value.config is T }.values.firstOrNull() as Config<T>?

    /**
     * Gets a config object from the [configs] map by the type [T].
     *
     * @param T The type of the config.
     * @return The config object with the type [T] or null if not found.
     */
    inline fun <reified T : Any> getConfigObject(): T? = getConfig<T>()?.config

    /**
     * Gets a config from the [configs] map by [name].
     *
     * @param name The name of the config.
     * @return The config with the name [name] or null if not found.
     */
    fun getConfig(name: String): Config<*>? = configs[name]

    /**
     * Gets all [configs].
     *
     * @return A map of all configs. The key is the name of the config (e.g. "keybinds"). The value is the [Config] object.
     */
    fun getConfigs(): MutableMap<String, Config<*>> = configs

    /**
     * Adds a config to the [configs] map.
     *
     * @param name The name of the config.
     * @param config The config object.
     */
    fun addConfig(name: String, config: Config<*>) {
        configs[name] = config
    }

    /**
     * Saves all configs that are in both [configs] and [configObj].
     *
     * @param configObj The config objects to save.
     */
    fun writeConfigs(vararg configObj: Any) =
        configs.forEach { (_, config) ->
            configObj.forEach innerForEach@{ obj ->
                if (obj == config.config) {
                    config.writeConfig()
                    return@innerForEach
                }
            }
        }

    inline fun <reified T : Any> delegateConfig(): DelegateHolder<Config<T>> = DelegateHolder(getConfig<T>()!!)

    companion object {
        inline fun <reified T : Any> delegateConfig(configManager: ConfigManager): DelegateHolder<Config<T>> = DelegateHolder(configManager.getConfig<T>()!!)
    }
}