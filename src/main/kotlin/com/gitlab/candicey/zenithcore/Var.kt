package com.gitlab.candicey.zenithcore

import com.gitlab.candicey.zenithcore.extension.toClassNode
import com.gitlab.candicey.zenithcore.helper.LunarClassHelper
import com.gitlab.candicey.zenithcore.helper.ScaledResolutionHelper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.FontRenderer
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.objectweb.asm.tree.ClassNode
import java.util.*

val ZENITH_CORE_VERSION: String = "@@ZENITH_CORE_VERSION@@"

internal val LOGGER: Logger by lazy { LogManager.getLogger("Zenith-Core v$ZENITH_CORE_VERSION") }

val HOME: String by lazy { System.getProperty("user.home") }

val RANDOM: Random by lazy { Random() }

val GSON: Gson by lazy { Gson() }
val GSON_PRETTY: Gson by lazy { GsonBuilder().setPrettyPrinting().create() }

val JSON_PARSER: JsonParser by lazy { JsonParser() }

val mc: Minecraft
    get() = Minecraft.getMinecraft()
val fontRenderer: FontRenderer
    get() = mc.fontRendererObj
val displayWidth: Int
    get() = mc.displayWidth
val displayHeight: Int
    get() = mc.displayHeight
val gameWidth: Int
    get() = ScaledResolutionHelper.scaledResolution.scaledWidth
val gameHeight: Int
    get() = ScaledResolutionHelper.scaledResolution.scaledHeight

/**
 * Regex for checking if a string is a valid Minecraft username. (3-16 characters, letters, numbers, and underscores only)
 */
val nameRegex: Regex by lazy { Regex("[a-zA-Z0-9_]{3,16}") }

val mainLoader: ClassLoader = ZenithCoreMain::class.java.classLoader

val lunarClasses: List<ClassNode> by lazy { LunarClassHelper.classBytes["lunar.jar"]!!.entries.map { it.value.toClassNode() } }