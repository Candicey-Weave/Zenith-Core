package com.gitlab.candicey.zenithcore.hook

import com.gitlab.candicey.zenithcore.extension.addFieldSafe
import net.weavemc.loader.api.Hook
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode

/**
 * Adds a field to the ClickEvent class that allows for the execution of a Runnable.
 *
 * The runnable is intended to be used with [com.gitlab.candicey.zenithcore.event.CustomClickEvent.NONE].
 *
 * @see [ClickEventActionHook]
 * @see [GuiScreenHook]
 */
object ClickEventHook : Hook("net/minecraft/event/ClickEvent") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.addFieldSafe(
            FieldNode(
                Opcodes.ACC_PUBLIC,
                "runnable",
                "Ljava/lang/Runnable;",
                null,
                null
            )
        )
    }
}