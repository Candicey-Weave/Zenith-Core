package com.gitlab.candicey.zenithcore.hook

import com.gitlab.candicey.zenithcore.extension.cancel
import com.gitlab.candicey.zenithcore.extension.networkManager
import com.gitlab.candicey.zenithcore.extension.previousGuiScreen
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zenithcore.util.callStaticsBooleanIf
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.multiplayer.GuiConnecting
import net.weavemc.loader.api.Hook
import org.objectweb.asm.tree.ClassNode

object GuiConnectingHook : Hook("net/minecraft/client/multiplayer/GuiConnecting") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStaticsBooleanIf<GuiConnectingHook>(
                "actionPerformed"
        )

        cfg.computeFrames()
    }

    /**
     * Lunar Client modifies the [GuiConnecting.actionPerformed] method to sometimes not close the gui when the cancel button is pressed.
     */
    @JvmStatic
    fun onActionPerformed(guiConnecting: GuiConnecting, button: GuiButton): Boolean {
        if (button.id == 0) {
            guiConnecting.cancel = true
            guiConnecting.networkManager?.closeChannel("Aborted".toChatComponent())
            mc.displayGuiScreen(guiConnecting.previousGuiScreen)

            return true
        }

        return false
    }
}