package com.gitlab.candicey.zenithcore.hook

import com.gitlab.candicey.zenithcore.extension.toDescriptor
import com.gitlab.candicey.zenithcore.util.callStaticsNotNull
import net.minecraft.client.resources.Locale
import net.weavemc.loader.api.Hook
import org.objectweb.asm.tree.ClassNode

object LocaleHook : Hook("net/minecraft/client/resources/Locale") {
    /**
     * A map of unlocalised strings to their localised counterparts.
     *
     * @see [com.gitlab.candicey.zenithcore.manager.KeyBindManager]
     */
    val messages: MutableMap<String, String> = mutableMapOf(
        // "zenithcore.keybind.test" to "Test KeyBind"
    )

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStaticsNotNull<LocaleHook>(
            "formatMessage" to String::class.java.toDescriptor()
        )
    }

    @JvmStatic
    fun onFormatMessage(locale: Locale, string: String, objects: Array<Any>): String? {
        return messages[string]
    }
}