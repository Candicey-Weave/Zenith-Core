package com.gitlab.candicey.zenithcore.hook

import com.gitlab.candicey.zenithcore.font.FontManager
import com.gitlab.candicey.zenithcore.info
import net.weavemc.loader.api.Hook
import org.objectweb.asm.tree.ClassNode

/**
 * Used to initialise the [FontManager].
 */
object GL11Hook : Hook("org/lwjgl/opengl/GL11") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        info("Initialising FontManager...")
        FontManager.init()
        info("FontManager initialised!")
    }
}