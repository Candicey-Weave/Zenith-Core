package com.gitlab.candicey.zenithcore.hook

import com.gitlab.candicey.zenithcore.keybind.KeyBindManager
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import net.minecraft.client.settings.GameSettings
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.apache.commons.lang3.ArrayUtils
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldInsnNode

/**
 * Adds the key bindings from [KeyBindManager] to the game settings.
 *
 * @see [com.gitlab.candicey.zenithcore.manager.KeyBindManager]
 */
internal object GameSettingsHook : Hook("net/minecraft/client/settings/GameSettings") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods
            .filter { it.name == "<init>" }
            .map { it.instructions }
            .forEach { insnList ->
                insnList.insert(insnList.find { abstractInsnNode ->
                    if (abstractInsnNode.opcode != Opcodes.PUTFIELD) {
                        return@find false
                    } else {
                        val fieldInsnNode = abstractInsnNode as FieldInsnNode
                        return@find fieldInsnNode.owner == "net/minecraft/client/settings/GameSettings" && fieldInsnNode.name == "keyBindings" && fieldInsnNode.desc == "[Lnet/minecraft/client/settings/KeyBinding;"
                    }
                }, asm {
                    aload(0)
                    invokestatic(
                        internalNameOf<GameSettingsHook>(),
                        "onInit",
                        "(Lnet/minecraft/client/settings/GameSettings;)V"
                    )
                })
            }
    }

    @JvmStatic
    fun onInit(gameSettings: GameSettings) {
        gameSettings.keyBindings = ArrayUtils.addAll(gameSettings.keyBindings, *KeyBindManager.keyBindings.toTypedArray(), *KeyBindManager.lateKeyBindings.toTypedArray())
    }
}