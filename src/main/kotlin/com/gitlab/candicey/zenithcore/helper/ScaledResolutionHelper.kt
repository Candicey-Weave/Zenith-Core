package com.gitlab.candicey.zenithcore.helper

import com.gitlab.candicey.zenithcore.event.TickEvent
import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zenithcore.helper.ScaledResolutionHelper.scaledResolution
import net.minecraft.client.gui.ScaledResolution
import net.weavemc.loader.api.event.SubscribeEvent

/**
 * Updates the [scaledResolution] every tick.
 */
object ScaledResolutionHelper {
    lateinit var scaledResolution: ScaledResolution

    @SubscribeEvent
    fun onTick(event: TickEvent.Pre) {
        scaledResolution = ScaledResolution(mc)
    }
}