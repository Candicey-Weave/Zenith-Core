package com.gitlab.candicey.zenithcore.helper

import com.gitlab.candicey.zenithcore.util.reflectField
import net.weavemc.loader.api.Hook

/**
 * A helper class for accessing the [net.weavemc.loader.HookManager] class.
 *
 * @see [net.weavemc.loader.HookManager]
 */
object HookManagerHelper {
    private val hookManagerInstance = reflectField("net/weavemc/loader/HookManager", "INSTANCE").get()

    /**
     * A list of hooks that are registered.
     *
     * @see [net.weavemc.loader.HookManager.hooks]
     */
    val hooks = reflectField("net/weavemc/loader/HookManager", "hooks").get(hookManagerInstance) as MutableList<Hook>
}